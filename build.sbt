ThisBuild/scalaVersion := "2.12.6"
ThisBuild/scalacOptions := Seq (
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xcheckinit",
)

lazy val assign = (project in file("."))
  // Descriptors for Project
  .settings(
    name := "MQRC Report Generator",
    version := "0.1",

    cancelable in Scope.Global := true,
    libraryDependencies ++= Seq(
      "com.atlassian.commonmark" % "commonmark" % "0.11.0",
      "com.atlassian.commonmark" % "commonmark-ext-gfm-tables" % "[0.9.0,)",
      "com.atlassian.commonmark" % "commonmark-ext-heading-anchor" % "[0.9.0,)",
      "org.jsoup" % "jsoup" % "[1.11.3,)",
    )
  )
