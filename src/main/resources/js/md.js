/**
 * md.js
 *
 * A collection of all javascript based utility functions to be used by the project
 */

const ID_PREFIX = {
    CELL : "td-",
    TB_SUBMIT : "tb-sub-",
    TB: "tb-",
    OPT: "opt-"
};

// Elements that need to be changeable
const tableCells = document.querySelectorAll("[id^='"+ID_PREFIX.CELL+"']");
const textBoxSubmit = document.querySelectorAll("[id^='"+ID_PREFIX.TB_SUBMIT+"']");
const options = document.querySelectorAll("[id^='"+ID_PREFIX.OPT+"']");

// Deals with cell changing events
Array.from(tableCells).forEach( cell => {
    cell.addEventListener('click',
        function () {
            // Change colour class of the cell
            this.className = {
                green: 'yellow',
                yellow: 'red',
                red: 'green'
            }[this.className];

            // Tell JavaFx of the new class
            jsLinker.jsClickedCell(this);
            event.preventDefault();
            event.stopPropagation();
        }
    );
});

// Deals with box submission events 
Array.from(textBoxSubmit).forEach( box => {
    box.addEventListener('click',
        function () {
            const textBoxId = ID_PREFIX.TB + this.id.substr(ID_PREFIX.TB_SUBMIT.length);
            const textBox = document.getElementById(textBoxId);

            // Colour button green when submitted
            this.style.backgroundColor = "chartreuse";
            this.value = "It is now: " + textBox.value;

            // Tell JavaFx of the inputted value
            jsLinker.jsTextBoxSubmit(textBox);
            event.preventDefault();
            event.stopPropagation();
        }
    );
});

Array.from(options).forEach( option => {
    option.addEventListener('click',
        function () {
            // Change class of the option
            this.className = {
                selected: 'not-selected',
                'not-selected' : 'selected',
            }[this.className];

            // Tell JavaFx of the new class
            jsLinker.jsOptionClicked(this);
            event.preventDefault();
            event.stopPropagation();
        }
    );
});