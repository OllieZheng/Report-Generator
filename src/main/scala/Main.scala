import java.io.File

import javafx.application.Application
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.collections.{FXCollections, ObservableList}
import javafx.concurrent.Worker
import javafx.event.{ActionEvent, EventHandler}
import javafx.geometry.{HPos, Insets, Pos}
import javafx.scene.Scene
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.{Alert, Button, Label, ListView}
import javafx.scene.web.WebView
import javafx.scene.layout._
import javafx.stage.Stage
import netscape.javascript.JSObject

import scala.collection.JavaConverters._
import util.Utility._

class Main extends Application
{
  import Main._
  private var stage : Stage = new Stage

  override def start(primaryStage: Stage): Unit = {
    this.stage = primaryStage
    this.stage setTitle "MQRC"

    primaryStage setScene selectTestsScene
    primaryStage show()
  }

  def selectTestsScene () : Scene = {
    // Display
    val root = new BorderPane()
    val grid : GridPane = new GridPane
    grid setPadding new Insets(5)
    grid setHgap 10
    grid setVgap 10

    val columnAvailable : ColumnConstraints =
      new ColumnConstraints(300)
    val columnSelectionButtons : ColumnConstraints =
      new ColumnConstraints(100)
    val columnSelected : ColumnConstraints =
      new ColumnConstraints(300)

    columnAvailable.setHgrow(Priority.ALWAYS)
    columnSelected.setHgrow(Priority.ALWAYS)

    grid.getColumnConstraints
      .addAll(columnAvailable, columnSelectionButtons, columnSelected)

    // Display List Views
    val listAvailable : ListView[File] = new ListView[File]
    val itemsAvailable : ObservableList[File] = FXCollections.observableArrayList()

    // Populates available files
    dirHandle.txtFiles foreach itemsAvailable.add

    val listSelected : ListView[File] = new ListView[File]
    val itemsSelected : ObservableList[File] = FXCollections.observableArrayList()

    listAvailable setItems itemsAvailable
    listSelected setItems itemsSelected

    // Display Buttons
    val sendRightButton = new Button(" > ")
    sendRightButton.setOnAction((_ : ActionEvent) =>
      listAvailable.getSelectionModel.getSelectedItem match {
        case a if a == null =>
        case potential =>
          listAvailable.getSelectionModel.clearSelection()
          itemsAvailable.remove(potential)
          itemsSelected.add(potential)
      }
    )

    val sendLeftButton = new Button(" < ")
    sendLeftButton.setOnAction((_ : ActionEvent) =>
      listSelected.getSelectionModel.getSelectedItem match {
        case a if a == null =>
        case selection =>
          listSelected.getSelectionModel.clearSelection()
          itemsSelected.remove(selection)
          itemsAvailable.add(selection)
      }
    )

    val goToReportButton = new Button("Create Battery")
    goToReportButton.setOnAction((_ : ActionEvent) => {
      if (itemsSelected.isEmpty) {
        val alert = new Alert(AlertType.INFORMATION)
        alert.setTitle("Warning")
        alert.setHeaderText(null)
        alert.setContentText("You must select at least one test")
        alert.showAndWait()
      } else {
        dirHandle setFiles itemsSelected.asScala
        this.stage setScene ReportScene
      }
    })

    val cancelButton = new Button("Cancel")
    cancelButton.setOnAction((_ : ActionEvent) => stage close())

    val vbox : VBox = new VBox(5)
    vbox.getChildren.addAll(sendRightButton, sendLeftButton)
    vbox setAlignment Pos.CENTER

    val availableLabel : Label = new Label("Available Tests")
    val selectedLabel : Label = new Label("Selected Tests")

    grid add(availableLabel, 0, 1)
    grid add(listAvailable, 0, 2)
    grid add(vbox, 1, 2)
    grid add(selectedLabel, 2, 1)
    grid add(listSelected, 2, 2)
    grid add(cancelButton, 1, 3)
    grid add(goToReportButton, 2, 3)

    // Setting Label Alignments
    GridPane.setHalignment(availableLabel, HPos.CENTER)
    GridPane.setHalignment(selectedLabel, HPos.CENTER)

    grid setAlignment Pos.CENTER
    root setCenter grid
    GridPane setVgrow(root, Priority.ALWAYS)

    new Scene(root)
  }

  def ReportScene() : Scene = {
    val root = new BorderPane
    val grid = new GridPane

    val webView = new WebView()
    val webEngine = webView.getEngine

    // Allows us to listen to JavaScript calls
    webEngine.setJavaScriptEnabled(true)

    webEngine.getLoadWorker.stateProperty.addListener(
      new ChangeListener[Worker.State] {
        override def changed(observable: ObservableValue[_ <: Worker.State],
                             oldState: Worker.State,
                             newState: Worker.State): Unit =
          if (newState == Worker.State.SUCCEEDED) {
            val window = webEngine.executeScript("window").asInstanceOf[JSObject]
            // Runs the Javascript code
            webEngine.executeScript(jsCode)
            // Javascript will now recognise jsLinker as a variable
            // and javafx will allow us to receive javascript input in java
            window.setMember("jsLinker", dirHandle.jsLinker)
          }
      }
    )

    val cancelButton : Button = new Button("Cancel")
    cancelButton.setOnAction((_ : ActionEvent) => stage close())

    val nextFileButton : Button = new Button("Next")
    nextFileButton.setOnAction((_ : ActionEvent) => {
      dirHandle decorateCurrent()
      if (dirHandle.selectedFiles.hasNext)
        this.stage setScene ReportScene
      else {
        val file = dirHandle.completed.mkString
        import java.io.PrintWriter
        new PrintWriter("report.html") { write(file); close }
        stage close()
      }
    })

    val html : String = cssCode + dirHandle.translateCurrent()
    webEngine loadContent html

    grid add (webView, 0, 0)
    grid add (cancelButton, 0, 1)
    grid add (nextFileButton, 1, 1)
    root setCenter grid

    new Scene(root)
  }
}


object Main
{
  // Retrieve css styling
  // This method must be used as using `<link>` won't work
  // as the html being read is only a format and not a file
  val cssCell : String = resMkString("/css/md.css")
  val cssCode : String = addTag(cssCell, "style")

  // Retrieve javascript code
  // This gets run by the javaFx WebEngine not the html
  val jsCode : String = resMkString("/js/md.js")

  val dirHandle = new DirectoryHandler("tests")

  def main(args: Array[String]): Unit = {
    Application.launch(classOf[Main], args: _*)
  }
}