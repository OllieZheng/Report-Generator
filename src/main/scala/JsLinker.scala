/**
  * JsLinker.scala
  *
  * Class that listens to all WebEngine JavaScript input
  */
import org.w3c.dom.html.{HTMLElement, HTMLInputElement}

import util.Globals._

class JsLinker
{
  var cellsClicked : Map[String, String] = Map()
  var textBoxValues : Map[String, String] = Map()
  var optionsClicked : Map[String, String] = Map()

  /**
    * Handles the table cell click event from JS
    */
  def jsClickedCell(obj : Object) : Unit =
    obj match {
      case nots : HTMLElement if nots.getClassName == DEFAULT_CELL_CLASS =>
        cellsClicked -= nots.getId
      case sel : HTMLElement =>
        cellsClicked += (sel.getId -> sel.getClassName)
    }

  /**
    * Handles the text box submission click event from JS
    */
  def jsTextBoxSubmit(obj : Object) : Unit = obj match {
    case input : HTMLInputElement =>
      textBoxValues += (input.getId -> input.getValue)
  }

  /**
    * Handles the optional value click event from JS
    */
  def jsOptionClicked(obj : Object) : Unit =
    obj match {
      case nots : HTMLElement if nots.getClassName == DEFAULT_OPTION =>
        optionsClicked -= nots.getId
      case sel : HTMLElement =>
        optionsClicked += (sel.getId -> sel.getClassName)
    }

  def resetAttributes () : Unit = {
    this.cellsClicked = Map()
    this.textBoxValues = Map()
    this.optionsClicked = Map()
  }
}

object JsLinker