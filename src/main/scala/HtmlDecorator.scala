import org.jsoup.Jsoup
import org.jsoup.nodes.{Element, Document, TextNode}
import scala.collection.JavaConverters._

import util.Globals._

/**
  * Uses JSoup to change HTML attributes
  */
class HtmlDecorator
{
  var html : String = _
  var document : Document = _

  def setDocument (html : String) : Unit = this.document = Jsoup.parse(html)

  /**
    * Changes an html id attribute's class by a map
    *
    * @param map where `._1` is the id, `._2` is the intended class
    */
  def setIdClasses (map: Map[String, String]) : Unit = 
    map foreach (
      row => document.getElementById(row._1).attr("class", row._2)
    )

  /**
    * Replaces all input boxes with what ever text was submitted
    *
    * @param map where `._1` is the textbox id, `._2` is the value
    */
  def setTextBoxValues (map: Map[String, String]) : Unit = {
    println(map)
    print(document.html())
    map foreach (
      row => 
        document.getElementById( row._1 )
          .replaceWith(new TextNode(row._2))
    )
    document.getElementsByTag("input").remove()
  }

  def setSelectedOptions(map: Map[String, String]) : Unit = {
    val options = document.select("[id^='" + OPT_ID_PREFIX + "']").asScala
    document.getElementsByClass("deleteThis").remove
    for ( opt : Element <- options )
      if (!map.contains(opt.id))
        opt remove()
      else
        opt clearAttributes()
  }
}

object HtmlDecorator
