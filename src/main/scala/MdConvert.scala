import java.io.File
import java.util.{InputMismatchException, Set => JSet}

import org.commonmark.ext.gfm.tables.{TableCell, TablesExtension}
import org.commonmark.ext.heading.anchor.IdGenerator
import org.commonmark.node._
import org.commonmark.parser.Parser
import org.commonmark.parser.delimiter.{DelimiterProcessor, DelimiterRun}
import org.commonmark.renderer.html._
import org.commonmark.renderer.NodeRenderer

import scala.collection.JavaConverters._
import scala.io.Source

import util.Globals._

/**
  * MdConvert.scala
  *
  * This class contains all the relevant information and the html conversion
  * of the markdown file being accessed
  */
class MdConvert
{
  private var file : File = _
  private val extensions = List(
    // Includes the ability to parse markdown tables
    TablesExtension.create(),
  ).asJava

  private val parser : Parser = Parser.builder()
    // Lets `commonmark` identify markdown tables
    .extensions(extensions)
    // Lets `commonmark` identify text input delimitation
    .customDelimiterProcessor(new MdConvert.TextInputProcessor)
    .build()

  private val renderer : HtmlRenderer = HtmlRenderer.builder()
    // Assigns table cells with custom ids and sets their class to default colour
    .nodeRendererFactory(
      new HtmlNodeRendererFactory() {
        override def create(context: HtmlNodeRendererContext): NodeRenderer =
          new MdConvert.CustomRenderer(context)
      }
    )
    .extensions(extensions)
    .build()

  /**
    * Creates the HTML conversion of the markdown file being accessed
    */
  def toHtml : String = {
    val file = Source.fromFile(this.file)
    val fileStr : String = file.mkString
    file close()
    renderer render(parser parse fileStr)
  }

  def setMdFile(file : File) : Unit = this.file = file
}

object MdConvert
{
  /**
    * Local function which travels down the commonmark nodes
    * and gets the string literal,
    * Means that text formatting is not supported in table cells
    */
  private def renderLiteral(node: Node): String =
    node match {
      case t: Text => t.getLiteral
      case n: Node => renderLiteral(n.getFirstChild)
      case _ => throw new InputMismatchException
    }

  /**
    * Local function which travels down the commonmark node
    * and renders the children
    */
  private def renderChildren(node: Node, context: HtmlNodeRendererContext): Unit =
    node.getFirstChild match {
      case a if a == null =>
      case n : Node =>
        context.render(n)
        n.getNext
    }

  /**
    * Custom html renderer for this project
    *
    * Creates custom green coloured tables and user input text boxes
    */
  private class CustomRenderer(context: HtmlNodeRendererContext)
    extends NodeRenderer {
    private val html: HtmlWriter = context.getWriter

    // Id generators for every html element
    private val idGenerators : Map[String, IdGenerator] = Map(
      "cell" -> IdGenerator.builder
        .prefix(CELL_ID_PREFIX),
      "tb" -> IdGenerator.builder
        .prefix(TB_ID_PREFIX),
      "tbSub" -> IdGenerator.builder
        .prefix(TB_SUB_ID_PREFIX),
      "opt" -> IdGenerator.builder
        .prefix(OPT_ID_PREFIX),
    ).transform((_,builder) => builder.build())

    override def getNodeTypes: JSet[Class[_ <: Node]] =
      Set[Class[_ <: Node]](
        classOf[TableCell],
        classOf[TextInput],
        classOf[Link],
      ).asJava

    override def render(node: Node): Unit = {
      val elemRender : BasicInlineSetter = new BasicInlineSetter(node, context)

      node match {
        case _ : TableCell =>
          elemRender.setId(idGenerators("cell"))
          elemRender.setAttributes("class" -> "green")

          // Formats the block to our desired tag
          html.tag("td", elemRender.attributes.asJava)
          elemRender.children
          html.tag("/td")

        case _ : TextInput =>
          elemRender.setId(idGenerators("tb"))
          elemRender.setAttributes("type"->"text")

          html.tag("input", elemRender.attributes.asJava)
          elemRender.setId(idGenerators("tbSub"))
          elemRender.setAttributes("type"->"submit")
          html.tag("input", elemRender.attributes.asJava)

        case link : Link =>
          val linkRender = new LinkSetter(node, context)

          linkRender.setId(idGenerators("opt"))
          linkRender.setAttributes("class" -> "not-selected")
          html.tag("span", Map("class" -> "deleteThis").asJava)
          html.text("(Select text:) ")
          html.tag("/span")

          html.tag("span", linkRender.attributes.asJava)
          html.text(linkRender.literal + " ")
          html.tag("/span")

          linkRender.setId(idGenerators("opt"))
          html.tag("span", linkRender.attributes.asJava)
          html.text(link.getTitle + " ")
          html.tag("/span")
      }
    }
  }

  /**
    * Tells commonmark that "++" is a delimiter for an element
    *
    * For this project, "++" denotes a text box and corresponding button
    * where users can enter values
    *
    * E.g. ++text box id++ creates a text box with id=tb-text-box-id
    */
  private class TextInput extends CustomNode with Delimited {
    override def getOpeningDelimiter: String = "+{"
    override def getClosingDelimiter: String = "}+"
  }

  /**
    * Custom processor for commonmark which deals with how `TextInput`
    * handles the delimiter
    */
  private class TextInputProcessor extends DelimiterProcessor {
    private val secondCharacter : Char = '{'
    private val secondLastCharacter : Char = '}'

    override def getOpeningCharacter: Char = '+'
    override def getClosingCharacter: Char = '+'

    // Minimum number of delimiter characters on each side to activate the delimiter
    override def getMinLength: Int = 1
    // Determine how many delimiter characters should be used
    override def getDelimiterUse(opener: DelimiterRun, closer: DelimiterRun): Int =
      if (opener.length() >= 1 && closer.length() >= 1) 1 else 0

    // Processes the recognised delimiter
    override def process(opener: Text, closer: Text, delimiterCount: Int) : Unit = {
      val textInput : Node = new TextInput
      var tmp : Node = opener.getNext

      tmp match {
        case _ : Node with Delimited =>
        case _ =>
          val literal = renderLiteral(tmp)
          // Reject delimiters that aren't followed by '{' and '}'
          if (literal.head == secondCharacter ||
              literal.last == secondLastCharacter ) {
            while (tmp != null && tmp != closer) {
              textInput.appendChild(tmp)
              tmp = tmp.getNext
            }
            opener.insertAfter(textInput)
          }
        }
    }
  }

  abstract class AttributeSetter(node : Node, context : HtmlNodeRendererContext) {
    var attributes : Map[String, String] = Map()
    lazy val children : Unit = renderChildren(this.node, context)
    lazy val literal : String = renderLiteral(this.node)

    def setId (idGenerator: IdGenerator)
    def setAttributes(attributes : (String, String)*): Unit =
      for (attribute <- attributes)
        this.attributes += attribute
  }

  class BasicInlineSetter(n : Node, c : HtmlNodeRendererContext) extends AttributeSetter (n, c) {
    def setId(idGenerator: IdGenerator) : Unit =
      this.attributes += ("id" -> idGenerator.generateId(literal))
  }

  class LinkSetter(node: Node, context: HtmlNodeRendererContext)
    extends AttributeSetter (node, context)
  {
    private var next_id_suffix : Int = 0
    private var default_id : String = _

    def setId(idGenerator : IdGenerator) : Unit = {
      if (default_id == null)
        this.default_id = idGenerator.generateId("")

      val current_id = this.default_id + "-" + next_id_suffix.toString
      this.attributes += ("id" -> current_id)
      next_id_suffix += 1
    }
  }
}