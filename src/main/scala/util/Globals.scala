package util

object Globals {
  // Default cell attribute values
  val DEFAULT_CELL_CLASS = "green"
  val CELL_ID_PREFIX = "td-"

  // Default text box attribute values
  val TB_ID_PREFIX = "tb-"
  val TB_SUB_ID_PREFIX = "tb-sub-"

  // Default optional sentence attribute values
  val OPT_ID_PREFIX = "opt-"
  val DEFAULT_OPTION = "not-selected"
}
