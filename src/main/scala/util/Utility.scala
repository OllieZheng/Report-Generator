package util

import scala.io.Source.fromInputStream

object Utility {
  /**
    * Takes a list of strings (ideally css styling) appends the strings together
    * and turns it into a valid html block
    *
    * @param css list of string of text
    * @param tag html tag which you want to allocate the string to
    * @return text encapsulated by the specified html tag
    */
  def addTag(css : List[String], tag : String) : String =
    "<" + tag + ">\n" + (css mkString "\n") + "\n</" + tag + ">"

  /**
    * Takes a string (ideally css styling) and turns it into a
    * valid html `<style>` block
    *
    * @param css string of text
    * @param tag html tag which you want to allocate the string to
    * @return text encapsulated by the specified html tag
    */
  def addTag(css : String, tag : String) : String =
    "<" + tag + ">\n" + css + "\n</" + tag + ">"

  /**
    * Takes file from `/src/main/resources/` and converts it to a string
    *
    * @param filePath file path of object in `/src/main/resources/`
    * @return the file contents as a string
    */
  def resMkString (filePath : String) : String = {
    val stream = getClass getResourceAsStream filePath
    fromInputStream(stream).mkString
  }
}
