import java.io.{File, FilenameFilter}

class DirectoryHandler(dir : String)
{
  private val mdConvert : MdConvert = new MdConvert
  private val htmlDecorator : HtmlDecorator = new HtmlDecorator
  var selectedFiles : Iterator[File] = Iterator.empty
  var completed : Seq[String] = Nil
  val jsLinker : JsLinker = new JsLinker

  lazy val txtFiles : List[File] = new File(dir).listFiles(
    new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = {
        if (name.lastIndexOf('.') > 0) {
          val lastIndex = name.lastIndexOf('.')
          return name.substring(lastIndex) == ".txt"
        }
        false
      }
    }
  ).toList

  def setFiles (files : Seq[File]) : Unit = {
    selectedFiles = files.toIterator
  }

  def translateCurrent() : String = {
    this.jsLinker resetAttributes()
    this.mdConvert setMdFile selectedFiles.next
    this.htmlDecorator.html = mdConvert.toHtml
    this.htmlDecorator.setDocument(this.htmlDecorator.html)

    this.htmlDecorator.html
  }

  def decorateCurrent() : Unit = {

    htmlDecorator.setIdClasses(jsLinker.cellsClicked)
    htmlDecorator.setTextBoxValues(jsLinker.textBoxValues)
    htmlDecorator.setSelectedOptions(jsLinker.optionsClicked)
    this.completed = this.completed :+ htmlDecorator.document.html
  }
}
