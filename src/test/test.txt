# I'm ALIVE

* Letter identification is important because a child cannot read a word if they cannot recognise the letters.
* We used the Cross-Case Copying (CCC) Task to test how well your child can identify letters.
* In this test, your child was asked to write the upper-case version of a lower-case letter (e.g. write Q when they saw q), or lower-case version of an upper-case letter (e.g. write B when they saw b).
* This test is an error test (see page 6 for a description of error tests).
* Below is a table showing the letters that your child correctly identified and wrote (highlighted in green) and did not correctly identify and write (highlighted in red).

++a++/12

+{a}+/12

+{a}+/12

+{a}+/12

+a+/12

{a}/12

{{a}}/12

| **a** | _b_ | 1 half |
| --- | --- | --- |
| a | d | 2 |
| a | f | 3 |

[aaa]: /HeyJude "Don't make it bad"
[aaa]

[Hey Jude](/heyjude "Don't make it bad")
